const path = require("path");
const { getAllFoodService } = require("../service/food.service");
exports.getAllFoodController = async (req, res, next) => {
  try {
    const result = await getAllFoodService(req);
    // return next(result);
    return next(result);
  } catch (error) {
    return next(error);
  }
};
