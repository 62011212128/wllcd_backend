exports.setResponseBody = (code, result, error) => {
  return {
    code: code,
    result: result,
    error: error,
  };
};
exports.setResponseSuccess = (result) => {
  return this.setResponseBody(200, result, null);
};
exports.setResponseError = (error) => {
  return this.setResponseBody(400, null, error);
};
exports.setResponseNotFound = () => {
  return this.setResponseBody(204, null, null);
};
