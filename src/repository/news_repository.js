const { connect } = require("../config/database");
let client = connect();
async function news() {
    let news = await client.query(
      `SELECT * FROM lose_weight.news;`
    );
    return news.rows;
  }
  module.exports=news;