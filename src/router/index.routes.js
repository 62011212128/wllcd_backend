const { Router } = require("express");
const router = Router({ mergeParams: true });
const foodRoute = require("./food.route");

router.use("/food", foodRoute);

module.exports = router;
