const { Router } = require("express");
const { getAllFoodController } = require("../controller/food.controller");
const router = Router({ mergeParams: true });

router.get("", getAllFoodController);

module.exports = router;
