const path = require("path");
exports.responseHandler = (res_data, request, response, next) => {
  response.header("Content-Type", "text/json");
  let json_data =
    res_data.code == 200
      ? {
          code: res_data.code,
          result: res_data.result,
        }
      : {
          code: res_data.code,
          result: res_data.result,
          error: res_data.error,
        };
  response
    .status(
      !res_data.code
        ? 500
        : res_data.code == 200 || res_data.code == 204
        ? 200
        : res_data.code
    )
    .json(json_data);
  return response;
};
