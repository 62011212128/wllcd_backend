const path = require("path");
const { findAllFood } = require("../repository/food_repository");
const {
  setResponseSuccess,
  setResponseError,
  setResponseNotFound,
} = require("../utils/response.util");
exports.getAllFoodService = async (req) => {
  try {
    // let foodAll = await findAllFood();
    let value = req.query.type || "";
    let foodAll = await findAllFood(`${value}`);
    if (foodAll.length == 0) {
      return setResponseNotFound();
    }
    return setResponseSuccess(foodAll);
  } catch (error) {
    console.log(error);
    return setResponseError(error);
  }
};
