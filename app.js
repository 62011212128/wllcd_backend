const connection = require("./src/config/database").connect();
const express = require("express");
const { responseHandler } = require("./src/middleware/response.middleware");
const { findAllFood, news } = require("./src/repository/food_repository");
const index_route = require("./src/router/index.routes");
const app = express();
app.use(express.json());

// app.get("/food", async (req, res) => {
//   let value = req.query.type || "";
//   let test = await findAllFood(`${value}`);
//   res.json(test);
// });
// app.get("/news", async (req, res) => {
//   let test = await news();
//   res.json(test);
// });
// Routes
app.use("/", index_route);
app.use(responseHandler)
module.exports = app;
